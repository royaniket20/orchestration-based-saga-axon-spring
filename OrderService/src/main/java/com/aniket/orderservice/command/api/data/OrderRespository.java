package com.aniket.orderservice.command.api.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRespository extends CrudRepository<Order , String> {
}
