package com.aniket.orderservice.command.api.saga;

import com.aniket.commonsservice.commands.*;
import com.aniket.commonsservice.event.*;
import com.aniket.commonsservice.model.UserDetails;
import com.aniket.commonsservice.quaries.GetUserPaymentDetailsQuery;
import com.aniket.orderservice.command.api.event.OrderCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.modelling.saga.EndSaga;
import org.axonframework.modelling.saga.SagaEventHandler;
import org.axonframework.modelling.saga.StartSaga;
import org.axonframework.queryhandling.QueryGateway;
import org.axonframework.spring.stereotype.Saga;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

@Saga
@Slf4j
public class OrderProcessingSaga {

    @Autowired
    private CommandGateway commandGateway;
    @Autowired
    private QueryGateway queryGateway;
    
    @StartSaga
    @SagaEventHandler(associationProperty = "orderId")
    private void handle(OrderCreatedEvent orderCreatedEvent)
    {
        log.info("ORDER SAGA STARTED");
        log.info("Order created Event Saga executing for the Order Id : {}",orderCreatedEvent.getOrderId());
        UserDetails userDetails = null;
        GetUserPaymentDetailsQuery getUserPaymentDetailsQuery = new GetUserPaymentDetailsQuery(orderCreatedEvent.getUserId());
        try{
            userDetails = queryGateway.query(getUserPaymentDetailsQuery, ResponseTypes.instanceOf(UserDetails.class)).join();
        }catch (Exception ex){
            log.error("Error is : {}",ex.getMessage());
            //Start compensating transaction when User Payment Information Not found
            cancleOrderCommandFiring(orderCreatedEvent.getOrderId());
        }

        ValidatePaymentCommand validatePaymentCommand =
                ValidatePaymentCommand.builder()
                        .cardDetails(userDetails.getCardDetails())
                        .orderId(orderCreatedEvent.getOrderId())
                        .paymentId(UUID.randomUUID().toString())
                        .build();

        commandGateway.sendAndWait(validatePaymentCommand);
    }

    private void cancleOrderCommandFiring(String orderId) {
        log.info("Cancleeing the Order for orderId = {}",orderId);
        CancelOrderCommand cancleOrderCommand = CancelOrderCommand.builder()
                .orderId(orderId)
                .orderStatus("CANCELLED")
                .build();

        commandGateway.send(cancleOrderCommand);

    }


    @SagaEventHandler(associationProperty = "orderId")
    private void handle(PaymentProcessEvent paymentProcessEvent)
    {
        log.info("Payment Processed Event Saga executing for the Order Id : {}",paymentProcessEvent.getOrderId());
        try {

            ShippingOrderCommand shippingOrderCommand = ShippingOrderCommand.builder()
                    .shipmentId(UUID.randomUUID().toString())
                    .orderId(paymentProcessEvent.getOrderId())
                    .build();
            throw new RuntimeException("Shipsment is having issue due to nation wide Strike");
           // commandGateway.send(shippingOrderCommand);
        }catch (Exception ex){
            log.error("Error is : {}",ex.getMessage());
            //Start compensating transaction
           canclePaymentCommandFiring(paymentProcessEvent);
        }
    }

    private void canclePaymentCommandFiring(PaymentProcessEvent paymentProcessEvent) {
        CancelPaymentCommand cancelPaymentCommand = CancelPaymentCommand.builder()
                .paymentStatus("CANCELLED")
                .orderId(paymentProcessEvent.getOrderId())
                .paymentId(paymentProcessEvent.getPaymentId())
                .build();

        commandGateway.send(cancelPaymentCommand);
    }


    @SagaEventHandler(associationProperty = "orderId")
    private void handle(OrderShippedEvent orderShippedEvent)
    {
        log.info("Order Shipped Processed Event Saga executing for the Order Id : {}",orderShippedEvent.getOrderId());
        try {
            CompleteOrderCommand completeOrderCommand = CompleteOrderCommand.builder()
                    .orderStatus("APPROVED")
                    .orderId(orderShippedEvent.getOrderId())
                    .build();
            commandGateway.send(completeOrderCommand);
        }catch (Exception ex){
            log.error("Error is : {}",ex.getMessage());
            //Start compensating transaction
            throw ex;
        }
    }


    @EndSaga
    @SagaEventHandler(associationProperty = "orderId")
    private void handle(OrderCompletedEvent orderCompletedEvent)
    {
        log.info("Order Completed  Event Saga executing for the Order Id : {}",orderCompletedEvent.getOrderId());
        try {
           log.info("As per diagram again we can Send email for order Confirmation etc etc .... ");

        }catch (Exception ex){
            log.error("Error is : {}",ex.getMessage());
            //Start compensating transaction
            throw ex;
        }
        log.info("ORDER SAGA COMPLETED !!!!");
    }



    @SagaEventHandler(associationProperty = "orderId")
    private void handle(OrderCancelledEvent orderCancelledEvent)
    {
        log.info("Order Cancelled  Event Saga executing for the Order Id : {}",orderCancelledEvent.getOrderId());
        try {
            log.info("As per diagram again we can Send Cancelationemail for order Confirmation etc etc .... ");

        }catch (Exception ex){
            log.error("Error is : {}",ex.getMessage());
            //Start compensating transaction
            throw ex;
        }
        log.info("ORDER SAGA COMPLETED WITH ROLLBACK !!!!");
    }

    @SagaEventHandler(associationProperty = "orderId")
    private void handle(PaymentCencelledEvent paymentCencelledEvent)
    {
        log.info("Payment Cancelled  Event Saga executing for the Order Id : {}",paymentCencelledEvent.getOrderId());
        cancleOrderCommandFiring(paymentCencelledEvent.getOrderId());

    }
}
