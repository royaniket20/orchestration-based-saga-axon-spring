package com.aniket.orderservice.command.api.controller;

import com.aniket.commonsservice.response.CommonResponse;
import com.aniket.orderservice.command.api.command.CreateOrderCommand;
import com.aniket.orderservice.command.api.model.OrderRestModel;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/orders")
public class OrderCommandController {

    @Autowired
    private CommandGateway commandGateway;

    @PostMapping
    public ResponseEntity<CommonResponse> createOrder(@RequestBody OrderRestModel orderRestModel)
    {
        String orderId = UUID.randomUUID().toString();
        String productId = UUID.randomUUID().toString();// Just for easy ness of experiment - Not real world use case
        String addressId = UUID.randomUUID().toString(); //// Just for easy ness of experiment - Not real world use case
        CreateOrderCommand createOrderCommand = CreateOrderCommand.builder()
                .orderId(orderId)
                .orderStatus("CREATED")
                .addressId(addressId)
                .productId(productId)
                .userId(orderRestModel.getUserId())
                .quantity(orderRestModel.getQuantity())
                .build();
        commandGateway.send(createOrderCommand);
        CommonResponse commonResponse= CommonResponse.builder()
            .Status(HttpStatus.ACCEPTED.value())
            .message("Order Created : "+orderId)
            .build();

        return new ResponseEntity<>(commonResponse,HttpStatus.ACCEPTED);
    }

}
