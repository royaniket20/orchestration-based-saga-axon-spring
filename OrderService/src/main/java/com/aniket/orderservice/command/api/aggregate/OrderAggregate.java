package com.aniket.orderservice.command.api.aggregate;

import com.aniket.commonsservice.commands.CancelOrderCommand;
import com.aniket.commonsservice.commands.CompleteOrderCommand;
import com.aniket.commonsservice.event.OrderCancelledEvent;
import com.aniket.commonsservice.event.OrderCompletedEvent;
import com.aniket.orderservice.command.api.command.CreateOrderCommand;
import com.aniket.orderservice.command.api.event.OrderCreatedEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.BeanUtils;

@Aggregate
@NoArgsConstructor
@AllArgsConstructor
@Data
public class OrderAggregate {

    @AggregateIdentifier
    private String orderId;
    private String productId;
    private String userId;
    private String addressId;
    private Integer quantity;
    private String orderStatus;

    @CommandHandler
    public OrderAggregate(CreateOrderCommand createOrderCommand) {

        //Validate the Command and send Event
        OrderCreatedEvent orderCreatedEvent = new OrderCreatedEvent();
        BeanUtils.copyProperties(createOrderCommand,orderCreatedEvent);
        AggregateLifecycle.apply(orderCreatedEvent);
    }

    @EventSourcingHandler
    public void on(OrderCreatedEvent orderCreatedEvent){

        //updating values in Aggregate

         this.orderStatus = orderCreatedEvent.getOrderStatus();
         this.orderId=orderCreatedEvent.getOrderId();
         this.addressId = orderCreatedEvent.getAddressId();
         this.productId = orderCreatedEvent.getProductId();
         this.quantity = orderCreatedEvent.getQuantity();
         this.userId = orderCreatedEvent.getUserId();
    }

    @CommandHandler
    public void handle(CompleteOrderCommand completeOrderCommand) {

        //Validate the Command and send Event
        OrderCompletedEvent  orderCompletedEvent =  OrderCompletedEvent.builder().build();
        BeanUtils.copyProperties(completeOrderCommand,orderCompletedEvent);
        AggregateLifecycle.apply(orderCompletedEvent);
    }

    @EventSourcingHandler
    public void on(OrderCompletedEvent orderCompletedEvent){

        //updating values in Aggregate

        this.orderStatus = orderCompletedEvent.getOrderStatus();
        //this.orderId=orderCompletedEvent.getOrderId(); //- Not changed
    }

    @CommandHandler
    public void handle(CancelOrderCommand cancleOrderCommand) {

        //Validate the Command and send Event
        OrderCancelledEvent  orderCancelledEvent =  OrderCancelledEvent.builder().build();
        BeanUtils.copyProperties(cancleOrderCommand,orderCancelledEvent);
        AggregateLifecycle.apply(orderCancelledEvent);
    }

    @EventSourcingHandler
    public void on(OrderCancelledEvent orderCancelledEvent){

        //updating values in Aggregate

        this.orderStatus = orderCancelledEvent.getOrderStatus();
        //this.orderId=orderCompletedEvent.getOrderId(); //- Not changed
    }

}
