package com.aniket.orderservice.command.api.event.handler;

import com.aniket.commonsservice.event.OrderCancelledEvent;
import com.aniket.commonsservice.event.OrderCompletedEvent;
import com.aniket.orderservice.command.api.data.Order;
import com.aniket.orderservice.command.api.data.OrderRespository;
import com.aniket.orderservice.command.api.event.OrderCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class OrderEventsHandler {

    @Autowired
    private OrderRespository orderRespository;

    @EventHandler
    public void on (OrderCreatedEvent orderCreatedEvent){
        Order order = new Order();
        BeanUtils.copyProperties(orderCreatedEvent,order);
        orderRespository.save(order);
        log.info("Order has been successfully Created - : {}",order);
    }

    @EventHandler
    public void on (OrderCompletedEvent orderCompletedEvent){
        Optional<Order> order =orderRespository.findById(orderCompletedEvent.getOrderId());
        if(order.isPresent()){
            Order actualOrder = order.get();
            actualOrder.setOrderStatus(orderCompletedEvent.getOrderStatus());
            orderRespository.save(actualOrder);
            log.info("Order has been successfully Completed - : {}",order);
        }

    }

    @EventHandler
    public void on (OrderCancelledEvent orderCancelledEvent){
        Optional<Order> order =orderRespository.findById(orderCancelledEvent.getOrderId());
        if(order.isPresent()){
            Order actualOrder = order.get();
            actualOrder.setOrderStatus(orderCancelledEvent.getOrderStatus());
            orderRespository.save(actualOrder);
            log.info("Order has been successfully Cancelled - : {}",order);
        }

    }

}
