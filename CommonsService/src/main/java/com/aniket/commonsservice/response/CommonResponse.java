package com.aniket.commonsservice.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CommonResponse {
    int Status;
    String message;
}
