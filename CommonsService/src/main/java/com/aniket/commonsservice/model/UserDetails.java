package com.aniket.commonsservice.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserDetails {
    private String userId;
    private String firstName;
    private String lastName;
    private CardDetails cardDetails;
}
