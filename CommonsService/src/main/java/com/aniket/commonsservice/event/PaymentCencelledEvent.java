package com.aniket.commonsservice.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentCencelledEvent {


    private String orderId;
    private String paymentId;
    private String paymentStatus;

}
