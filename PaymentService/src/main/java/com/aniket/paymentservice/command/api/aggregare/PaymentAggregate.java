package com.aniket.paymentservice.command.api.aggregare;

import com.aniket.commonsservice.commands.CancelPaymentCommand;
import com.aniket.commonsservice.commands.ValidatePaymentCommand;
import com.aniket.commonsservice.event.PaymentCencelledEvent;
import com.aniket.commonsservice.event.PaymentProcessEvent;
import com.aniket.commonsservice.model.CardDetails;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

@Slf4j
@Aggregate
@NoArgsConstructor
@AllArgsConstructor
public class PaymentAggregate {

    @AggregateIdentifier
    private String paymentId;
    private String orderId;
    private CardDetails cardDetails;
    private String paymentStatus;

    @CommandHandler
    public PaymentAggregate(ValidatePaymentCommand validatePaymentCommand){
        log.info("Validating the Payment Stuff and then Firing Payment Processed Event - For Payload : {}",validatePaymentCommand);
        PaymentProcessEvent paymentProcessEvent = PaymentProcessEvent.builder()
                .orderId(validatePaymentCommand.getOrderId())
                .paymentId(validatePaymentCommand.getPaymentId())
                .build();

                AggregateLifecycle.apply(paymentProcessEvent);
                log.info("Payment Processed Event fired");
    }

    @EventSourcingHandler
    public void on ( PaymentProcessEvent paymentProcessEvent){
   this.paymentId = paymentProcessEvent.getPaymentId();
   this.orderId  = paymentProcessEvent.getOrderId();
    }

    @CommandHandler
    public void handle(CancelPaymentCommand cancelPaymentCommand){
        log.info("Validating the Payment Cancellation Stuff and then Firing Payment Cancellation Processed Event - For Payload : {}",cancelPaymentCommand);
        PaymentCencelledEvent paymentCencelledEvent = PaymentCencelledEvent.builder()
                .orderId(cancelPaymentCommand.getOrderId())
                .paymentId(cancelPaymentCommand.getPaymentId())
                .paymentStatus(cancelPaymentCommand.getPaymentStatus())
                .build();

        AggregateLifecycle.apply(paymentCencelledEvent);
        log.info("Payment Cancellation Processed Event fired");
    }

    @EventSourcingHandler
    public void on ( PaymentCencelledEvent paymentCencelledEvent){
        this.paymentId = paymentCencelledEvent.getPaymentId();
        this.orderId  = paymentCencelledEvent.getOrderId();
        this.paymentStatus = paymentCencelledEvent.getPaymentStatus();
    }
}
