package com.aniket.paymentservice.command.api.event;

import com.aniket.commonsservice.event.PaymentCencelledEvent;
import com.aniket.commonsservice.event.PaymentProcessEvent;
import com.aniket.paymentservice.command.api.data.Payment;
import com.aniket.paymentservice.command.api.data.PaymentRepository;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Optional;

@Component
@Slf4j
public class PaymentEventHndler {


    @Autowired
    private PaymentRepository paymentRepository;
    @EventHandler
    public void on(PaymentProcessEvent paymentProcessEvent){
        Payment payment = Payment.builder()
                .paymentId(paymentProcessEvent.getPaymentId())
                .orderId(paymentProcessEvent.getOrderId())
                .timeStamp(new Date())
                .paymentStatus("COMPLETED")
                .build();
        paymentRepository.save(payment);
        log.info("Payment is Stored in Database");

    }

    @EventHandler
    public void on(PaymentCencelledEvent paymentCencelledEvent){
        Optional<Payment> payment =paymentRepository.findById(paymentCencelledEvent.getOrderId());
   if(payment.isPresent()) {
       Payment paymentActual = payment.get();
       paymentActual.setPaymentStatus(paymentCencelledEvent.getPaymentStatus());
       paymentRepository.save(paymentActual);
       log.info("Payment  Cancellation is Stored in Database");
   }

    }


}
