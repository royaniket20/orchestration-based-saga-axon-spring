package com.aniket.userservice.query.api.projection;

import com.aniket.commonsservice.model.CardDetails;
import com.aniket.commonsservice.model.UserDetails;
import com.aniket.commonsservice.quaries.GetUserPaymentDetailsQuery;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class UserProjection {

    @QueryHandler
    public UserDetails getUserPaymentDetails(GetUserPaymentDetailsQuery getUserPaymentDetailsQuery){
      log.info("**** Handling Query for the Query : {}",getUserPaymentDetailsQuery);
        CardDetails cardDetails = CardDetails.builder()
                .cardNumber("1122-3344-5566-7788")
                .cvv(123)
                .validUntilMonth(02)
                .validUntilYear(22)
                .build();
        UserDetails userDetails =   UserDetails.builder()
                .firstName("Aniket")
                .lastName("Roy")
                .cardDetails(cardDetails)
                .userId(getUserPaymentDetailsQuery.getUserId())
                .build();
        log.info("Final Payload from User service : {}",userDetails);
        return userDetails;
    }

}
