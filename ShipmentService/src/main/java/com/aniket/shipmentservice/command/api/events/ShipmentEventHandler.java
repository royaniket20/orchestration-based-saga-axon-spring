package com.aniket.shipmentservice.command.api.events;

import com.aniket.commonsservice.event.OrderShippedEvent;
import com.aniket.shipmentservice.command.api.data.Shipment;
import com.aniket.shipmentservice.command.api.data.ShipmentRepository;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Data
@Slf4j
@Component
public class ShipmentEventHandler {

    @Autowired
    private ShipmentRepository shipmentRepository;

    public void on(OrderShippedEvent orderShippedEvent) {
        log.info("Handing Event for the Event -- {}", orderShippedEvent);
        Shipment shipment = Shipment.builder().build();

        BeanUtils.copyProperties(orderShippedEvent,shipment);
        shipmentRepository.save(shipment);
        log.info("Shipment Information Now saved !!!");
    }
}
