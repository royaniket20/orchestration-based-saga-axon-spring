package com.aniket.shipmentservice.command.api.aggregate;


import com.aniket.commonsservice.commands.ShippingOrderCommand;
import com.aniket.commonsservice.event.OrderShippedEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

@Data
@Aggregate
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class ShipmentAggregate {

    @AggregateIdentifier
    private String shipmentId;
    private String orderId;
    private String shipmentStatus;

    @CommandHandler
    public ShipmentAggregate(ShippingOrderCommand shippingOrderCommand) {
   log.info("Here vlidate the Shipment and then Proceed to process it - {}",shippingOrderCommand);
        OrderShippedEvent orderShippedEvent = OrderShippedEvent.builder()
                .orderId(shippingOrderCommand.getOrderId())
                .shipmentId(shippingOrderCommand.getShipmentId())
                .shipmentStatus("ORDER SHIPPED")
                .build();
        AggregateLifecycle.apply(orderShippedEvent);
    log.info("Order Shipped Event Fired");
    }

    @EventSourcingHandler
    public void on(OrderShippedEvent orderShippedEvent){
       log.info("Doing Event Sourcing to make aggregate Updated");
       this.orderId = orderShippedEvent.getOrderId();
       this.shipmentId = orderShippedEvent.getShipmentId();
       this.shipmentStatus = orderShippedEvent.getShipmentStatus();
    }
}
